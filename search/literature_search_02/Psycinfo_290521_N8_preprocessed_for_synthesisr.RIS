﻿TY  - JOUR
AB  - The use of protective devices to safeguard health in noisy environments is crucial. Intensive exposure to pop, rock, and jazz (PRJ) music leads to irreversible damage to the auditory system. This study analyzed the psychological variables that best predict the use of personal hearing protection devices (PHPD) in professional PRJ musicians and the relative importance of PHPD in general as well as in four different occupational contexts, after controlling for auditory measures and condition. Professional musicians’ attitudes toward PHPD and behavior were assessed. Analyses showed that the best predictor of current usage was the duration of previous use, followed by additional psychological factors that predict usage in the four occupational settings. Researchers can draw on the current findings to explore other psychological factors and mechanisms that may facilitate the use of PHPD. For example, the finding that custom-made PHPD predicts the use of PHPD could be explored further by identifying the underlying psychological mechanisms, such as perception of ownership or identification processes. Studies should examine the other barriers/inducements to the use of PHPD in addition to those examined here, including the social factors connected to particular musical ensembles. Overall, psychological approaches encouraging behavioral change should be implemented, such as reminding individuals about previous use of PHPD, drawing their attention to these devices and the occupational environment, and enforcing PHPD norms to augment occupational health and safety. (PsycInfo Database Record (c) 2021 APA, all rights reserved)
AD  - Yaakobi, Erez, Faculty of Business Administration, Ono Academic College, Kiryat Ono, Israel
AN  - 2021-29502-003
AU  - Yaakobi, Erez
AU  - Putter-Katz, Hanna
DA  - Fal 2020
DB  - psyh
DO  - 10.5406/amerjpsyc.133.3.0313
DP  - EBSCOhost
IS  - 3
KW  - Behavioral change
Health protective devices
Health-related behaviors
Noisy environments
Occupational health
Psychological interventions
Theory of planned behavior
hearing protection devices
Behavior Change
Intervention
Noise Effects
Safety Devices
Auditory Stimulation
Working Conditions
Reasoned Action
N1  - Ono Academic College, Kiryat Ono, Israel. Release Date: 20210503. Publication Type: Journal (0100), Peer Reviewed Journal (0110). Format Covered: Electronic. Document Type: Journal Article. Language: EnglishMajor Descriptor: Behavior Change; Intervention; Noise Effects; Safety Devices; Occupational Health. Minor Descriptor: Auditory Stimulation; Working Conditions; Reasoned Action. Classification: Working Conditions & Industrial Safety (3670). Population: Human (10); Male (30); Female (40). Location: Israel. Age Group: Adulthood (18 yrs & older) (300); Young Adulthood (18-29 yrs) (320); Thirties (30-39 yrs) (340); Middle Age (40-64 yrs) (360). Methodology: Empirical Study; Quantitative Study. Supplemental Data: Tests Internet. Page Count: 15. Issue Publication Date: Fal 2020. Copyright Statement: The Board of Trustees of the University of Illinois. 2020.
PY  - 2020
SN  - 0002-9556
1939-8298
SP  - 313-327
ST  - What best predicts the use of hearing protection devices?
T2  - The American Journal of Psychology
TI  - What best predicts the use of hearing protection devices?
UR  - https://search.ebscohost.com/login.aspx?direct=true&db=psyh&AN=2021-29502-003&site=ehost-live
dr.yaakobi@ono.ac.il
VL  - 133
ID  - 1058
ER  - 
TY  - JOUR
AB  - Background: While previous studies indicate an auditory metronome can entrain cadence (in steps per minute), music may also evoke prescribed cadences and metabolic intensities. Purpose: To determine how modulating the tempo of a single commercial song influences adults’ ability to entrain foot strikes while walking and how this entrainment affects metabolic intensity. Methods: Twenty healthy adults (10 men and 10 women; mean [SD]: age 23.7 [2.7] y, height 172.8 [9.0] cm, mass 71.5 [16.2] kg) walked overground on a large circular pathway for six 5-min conditions; 3 self-selected speeds (slow, normal, and fast); and 3 trials listening to a song with its tempo modulated to 80, 100, and 125 beats per minute. During music trials, participants were instructed to synchronize their step timing with the music tempo. Cadence was measured via direct observation, and metabolic intensity (metabolic equivalents) was assessed using indirect calorimetry. Results: Participants entrained their cadences to the music tempos (mean absolute percentage error = 5.3% [5.8%]). Entraining to a music tempo of 100 beats per minute yielded ≥3 metabolic equivalents in 90% of participants. Trials with music entrainment exhibited greater metabolic intensity compared with self-paced trials (repeated-measures analysis of variance, F1,19 = 8.05, P = .01). Conclusion: This study demonstrates the potential for using music to evoke predictable metabolic intensities. (PsycInfo Database Record (c) 2020 APA, all rights reserved)
AD  - Tudor-Locke, Catrine
AN  - 2020-04483-015
AU  - Perry, Dylan C.
AU  - Moore, Christopher C.
AU  - Sands, Colleen J.
AU  - Aguiar, Elroy J.
AU  - Gould, Zachary R.
AU  - Tudor-Locke, Catrine
AU  - Ducharme, Scott W.
DB  - psyh
DO  - 10.1123/jpah.2019-0097
DP  - EBSCOhost
IS  - 11
KW  - gait
physical activity
prescription
rhythmic auditory cueing
steps per minute
Auditory Stimulation
Music
Tempo
Walking
Cues
Health Promotion
Rhythm
Stimulus Intensity
Test Construction
N1  - Physical Activity & Health Laboratory, Department of Kinesiology, University of Massachusetts Amherst, Amherst, MA, US. Release Date: 20201210. Publication Type: Journal (0100), Peer Reviewed Journal (0110). Format Covered: Electronic. Document Type: Journal Article. Language: EnglishMajor Descriptor: Auditory Stimulation; Music; Tempo; Walking; Gait. Minor Descriptor: Cues; Health Promotion; Rhythm; Stimulus Intensity; Test Construction. Classification: Promotion & Maintenance of Health & Wellness (3365); Recreation & Leisure (3740). Population: Human (10); Male (30); Female (40). Age Group: Adulthood (18 yrs & older) (300). Tests & Measures: Music Entrainment Accuracy Measure. Methodology: Empirical Study; Quantitative Study. Page Count: 8. Issue Publication Date: Nov, 2019. Copyright Statement: Human Kinetics, Inc. 2019.
PY  - 2019
SN  - 1543-3080
1543-5474
SP  - 1039-1046
ST  - Using music-based cadence entrainment to manipulate walking intensity
T2  - Journal of Physical Activity & Health
TI  - Using music-based cadence entrainment to manipulate walking intensity
UR  - https://search.ebscohost.com/login.aspx?direct=true&db=psyh&AN=2020-04483-015&site=ehost-live
Tudor-Locke@uncc.edu
VL  - 16
ID  - 1062
ER  - 
TY  - JOUR
AB  - Introduction: The Defence Forces’ members are exposed to high-level noise that increases their risk of hearing loss (HL). Besides military noise, the other risk factors include age and gender, ototoxic chemicals, vibration, and chronic stress. The current study was designed to study the effects of personal, work conditions-related risk factors, and other health-related traits on the presence of hearing problems. Materials and Methods: A cross-sectional study among active military service members was carried out. Altogether, 807 respondents completed a questionnaire about their health and personal and work-related risk factors in indoor and outdoor environments. The statistical analysis was performed using statistical package of social sciences (descriptive statistics) and R (correlation and regression analysis) software. Results: Almost half of the active service members reported HL during their service period. The most important risk factors predicting HL in the military appeared to be age, gender, and service duration. Also, working in a noisy environment with exposure to technological, vehicle, and impulse noise shows a statistically significant effect on hearing health. Moreover, we could identify the effect of stress on tinnitus and HL during the service period. Most importantly, active service members not using hearing protectors, tend to have more tinnitus than those who use it. Conclusions: The members of the Defence Forces experience noise from various sources, most of it resulting from outdoor activities. Personal and work conditions-related risk factors as well as stress increase the risk of hearing problems. (PsycInfo Database Record (c) 2021 APA, all rights reserved)
AD  - Luha, Assar, Institute of Technology, Estonian University of Life Sciences, Kreutzwaldi 56/1, Tartu, Estonia, 51006
AN  - 2021-10545-045
AU  - Luha, Assar
AU  - Kaart, Tanel
AU  - Merisalu, Eda
AU  - Indermitte, Ene
AU  - Orru, Hans
DB  - psyh
DO  - 10.1093/milmed/usaa224
DP  - EBSCOhost
IS  - 11-12
KW  - hearing problems
defence forces
occupational risk factors
Auditory Acuity
Environmental Effects
Noise Levels (Work Areas)
Risk Factors
Stress
Age Differences
Military Personnel
Social Sciences
Tinnitus
Vibration
N1  - Institute of Technology, Estonian University of Life Sciences, Tartu, Estonia. Other Publishers: Assn of Military Surgeons of the US. Release Date: 20210304. Publication Type: Journal (0100), Peer Reviewed Journal (0110). Format Covered: Electronic. Document Type: Journal Article. Language: EnglishGrant Information: Indermitte, Ene. Major Descriptor: Auditory Acuity; Environmental Effects; Noise Levels (Work Areas); Risk Factors; Stress. Minor Descriptor: Age Differences; Military Personnel; Social Sciences; Tinnitus; Vibration. Classification: Military Psychology (3800); Vision & Hearing & Sensory Disorders (3299). Population: Human (10); Male (30); Female (40). Location: Estonia. Age Group: Adulthood (18 yrs & older) (300). Tests & Measures: General Health Questionnaire-12 DOI: 10.1037/t00297-000. Methodology: Empirical Study; Quantitative Study. Supplemental Data: Tables and Figures Internet. Page Count: 9. Issue Publication Date: Nov-Dec, 2020. Copyright Statement: All rights reserved. Association of Military Surgeons of the United States. 2020.
Sponsor: Estonian Ministry of Defence, Estonia. Other Details: Under the project “Assessment and management of health risks among military personnel”. Recipients: No recipient indicated
Sponsor: Estonian Ministry of Education and Research, Estonia. Grant: IUT34-17. Recipients: Indermitte, Ene; Orru, Hans
PY  - 2020
SN  - 0026-4075
1930-613X
SP  - e2115-e2123
ST  - Hearing problems among the members of the defence forces in relation to personal and occupational risk factors
T2  - Military Medicine
TI  - Hearing problems among the members of the defence forces in relation to personal and occupational risk factors
UR  - https://search.ebscohost.com/login.aspx?direct=true&db=psyh&AN=2021-10545-045&site=ehost-live
assar.luha@emu.ee
VL  - 185
ID  - 1057
ER  - 
TY  - JOUR
AB  - Background and objectives: Maladaptive avoidance is a core characteristic of anxiety-related disorders. Its reduction is often promoted using extinction with response prevention (ExRP) procedures, but these effects are often short-lived. Research has shown that pairing a feared stimulus with a stimulus of an incompatible valence (i.e., counterconditioning) may be effective in reducing fear. This laboratory study tested whether positive imagery during ExRP (i.e., imagery counterconditioning protocol) can also reduce avoidance. Methods: In the counterconditioning procedure, participants imagined a positive sound. There were four phases. First, participants were presented with squares on a computer screen of which one (CS +) was paired with an aversive sound and another (CS-) was not. Second, they learned to avoid the negative sound in the presence of the CS +, via a key press. Third, they were assigned to either the Counterconditioning (that was asked to imagine a positive sound during ExRP) or No Counterconditioning group (standard ExRP). Finally, they performed a test phase that consisted of two parts: in the first part, avoidance responses were available for each CS and in the second part, these responses were prevented. Results: The Counterconditioning intervention resulted in a short-lived reduction of distress associated with the CS +. However, groups did not differ in avoidance or distress during the test phases. Limitations: US-expectancy ratings were collected only at the end of the experiment. Conclusions: The results indicate that positive imagery during ExRP may be effective in reducing distress during the intervention. Explanations for the persistence of avoidance and fear are discussed. (PsycInfo Database Record (c) 2021 APA, all rights reserved)
AD  - Krypotos, Angelos-Miltiadis, Department of Clinical Psychology, Utrecht University, PO Box 80140, 3508 TC, Utrecht, Netherlands
AN  - 2020-87485-001
AU  - Hendrikx, Laura J.
AU  - Krypotos, Angelos-Miltiadis
AU  - Engelhard, Iris M.
DB  - psyh
DO  - 10.1016/j.jbtep.2020.101601
DP  - EBSCOhost
KW  - anxiety Disorders
Exposure therapy
Counterconditioning
Auditory Stimulation
Avoidance
Distress
Imagery
Extinction (Learning)
Intervention
Prevention
Responses
N1  - Department of Clinical Psychology, Utrecht University, Utrecht, Netherlands. Release Date: 20210225. Publication Type: Journal (0100), Peer Reviewed Journal (0110). Format Covered: Electronic. Document Type: Journal Article. Language: EnglishGrant Information: Engelhard, Iris M. Major Descriptor: Auditory Stimulation; Avoidance; Counterconditioning; Distress; Imagery. Minor Descriptor: Extinction (Learning); Intervention; Prevention; Responses. Classification: Learning & Memory (2343). Population: Human (10); Male (30); Female (40). Location: Netherlands. Age Group: Adulthood (18 yrs & older) (300). Tests & Measures: Questionnaire Upon Mental Imagery; Visual Analogue Scale. Methodology: Empirical Study; Interview; Quantitative Study. Supplemental Data: Appendixes Internet. ArtID: 101601. Issue Publication Date: Mar, 2021. Publication History: First Posted Date: Aug 13, 2020; Accepted Date: Aug 1, 2020; Revised Date: Jul 13, 2020; First Submitted Date: Nov 2, 2019. Copyright Statement: Published by Elsevier Ltd. This is an open access article under the CC BY license (http://creativecommons.org/licenses/by/4.0/). The Authors. 2020.
Sponsor: Netherlands Organisation for Scientific Research, Netherlands. Grant: 453-15-005. Other Details: VICI grant. Recipients: Engelhard, Iris M.
PY  - 2021
SN  - 0005-7916
1873-7943
ST  - Enhancing extinction with response prevention via imagery-based counterconditioning: Results on conditioned avoidance and distress
T2  - Journal of Behavior Therapy and Experimental Psychiatry
TI  - Enhancing extinction with response prevention via imagery-based counterconditioning: Results on conditioned avoidance and distress
UR  - https://search.ebscohost.com/login.aspx?direct=true&db=psyh&AN=2020-87485-001&site=ehost-live
amkrypotos@gmail.com
VL  - 70
ID  - 1055
ER  - 
TY  - JOUR
AB  - Objective: In occupational hearing conservation programmes, age adjustments may be used to subtract expected age effects. Adjustments used in the U.S. came from a small dataset and overlooked important demographic factors, ages, and stimulus frequencies. The present study derived a set of population-based age adjustment tables and validated them using a database of exposed workers. Design: Cross-sectional population-based study and retrospective longitudinal cohort study for validation. Study sample: Data from the U.S. National Health and Nutrition Examination Survey (unweighted n = 9937) were used to produce these tables. Male firefighters and emergency medical service workers (76,195 audiograms) were used for validation. Results: Cross-sectional trends implied less change with age than assumed in current U.S. regulations. Different trends were observed among people identifying with non-Hispanic Black race/ethnicity. Four age adjustment tables (age range: 18–85) were developed (women or men; non-Hispanic Black or other race/ethnicity). Validation outcomes showed that the population-based tables matched median longitudinal changes in hearing sensitivity well. Conclusions: These population-based tables provide a suitable replacement for those implemented in current U.S. regulations. These tables address a broader range of worker ages, account for differences in hearing sensitivity across race/ethnicity categories, and have been validated for men using longitudinal data. (PsycInfo Database Record (c) 2020 APA, all rights reserved)
AD  - Flamme, Gregory A., Stephenson and Stephenson Research and Consulting, 2264 Heather Way, Forest Grove, OR, US, 97116
AN  - 2019-79468-001
AU  - Flamme, Gregory A.
AU  - Deiters, Kristy K.
AU  - Stephenson, Mark R.
AU  - Themann, Christa L.
AU  - Murphy, William J.
AU  - Byrne, David C.
AU  - Goldfarb, David G.
AU  - Zeig-Owens, Rachel
AU  - Hall, Charles
AU  - Prezant, David J.
AU  - Cone, James E.
DB  - psyh
DO  - 10.1080/14992027.2019.1698068
DP  - EBSCOhost
IS  - Suppl 1
KW  - Hearing conservation/hearing loss prevention
aging
noise
demographics/epidemiology
Age Differences
Prevention
Auditory Stimulation
Demographic Characteristics
Hearing Disorders
Occupational Health
N1  - Stephenson and Stephenson Research and Consulting (SASRAC), Forest Grove, OR, US. Other Publishers: Informa Healthcare. Release Date: 20191223. Correction Date: 20201203. Publication Type: Journal (0100), Peer Reviewed Journal (0110). Format Covered: Electronic. Document Type: Journal Article. Language: EnglishMajor Descriptor: Age Differences; Prevention. Minor Descriptor: Auditory Stimulation; Demographic Characteristics; Hearing Disorders; Occupational Health. Classification: Vision & Hearing & Sensory Disorders (3299). Population: Human (10); Male (30); Female (40). Location: US. Age Group: Adulthood (18 yrs & older) (300); Young Adulthood (18-29 yrs) (320); Thirties (30-39 yrs) (340); Middle Age (40-64 yrs) (360); Aged (65 yrs & older) (380); Very Old (85 yrs & older) (390). Methodology: Empirical Study; Longitudinal Study; Mathematical Model; Quantitative Study. Page Count: 11. Issue Publication Date: Feb, 2020. Publication History: Accepted Date: Nov 22, 2019; Revised Date: Nov 7, 2019; First Submitted Date: Jul 24, 2019.
Sponsor: Centers for Disease Control and Prevention/NIOSH. Grant: 214-2015-M-63254; 200-2013-M-57227. Recipients: No recipient indicated
Sponsor: NYC, DOHMH. Grant: 20182006861. Recipients: No recipient indicated
Sponsor: FDNY World Trade Center Programme Data Center. Grant: 200-2017-93326. Recipients: No recipient indicated
PY  - 2020
SN  - 1499-2027
1708-8186
SP  - S20-S30
ST  - Population-based age adjustment tables for use in occupational hearing conservation programs
T2  - International Journal of Audiology
TI  - Population-based age adjustment tables for use in occupational hearing conservation programs
UR  - https://search.ebscohost.com/login.aspx?direct=true&db=psyh&AN=2019-79468-001&site=ehost-live
ORCID: 0000-0002-8679-2306
ORCID: 0000-0003-2682-7543
ORCID: 0000-0002-9732-3353
ORCID: 0000-0002-2379-6609
gflamme@sasrac.com
VL  - 59
ID  - 1060
ER  - 
TY  - JOUR
AB  - Objectives: To engage with local primary care stakeholders to inform the model of care for a proposed academic integrative health care center incorporating evidence-informed traditional, complementary, and integrative medicine (TCIM) in Sydney, Australia. Design: In-depth semistructured interviews, informed by community-based participatory research principles, were conducted to explore primary care stakeholder preferences and service requirements regarding the proposed Western Sydney Integrative Health (WSIH) center in their local district. Setting: Telephone and face-to-face interviews at primary care clinics in Sydney. Subjects: Thirteen participants took part in the study: eight general practitioners (GPs) and five primary care practice managers (PMs). Methods: GPs were recruited through local GP newsletters, closed GP Facebook groups, and snowballing. PMs were recruited through a national PM newsletter. The semistructured interviews were audiorecorded and transcribed verbatim before conducting a thematic analysis. Results: Three main themes emerged: (1) the rationale for 'why' the WSIH center should be established, (2) 'what' was most important to provide, and (3) 'how' the center could achieve these goals. Participants were willing to refer to the service, acknowledging the demand for TCIM, current gaps in chronic disease care, and negligible Government funding for TCIM. They endorsed a model of care that minimizes out-of-pocket costs for the underserved, incorporates medical oversight, integrates evidence-informed TCIM with conventional health care, builds trust through interprofessional communication and education, and provides sound clinical governance with a strong focus on credentialing and risk management. It was proposed that safety and quality standards are best met by a GP-led approach and evidence-based practice. Conclusions: Our findings demonstrate that participants acknowledged the need for a model of care that fits into the local landscape through integrating conventional health care with TCIM in a team-based environment, with medical/GP oversight to ensure sound clinical governance. Findings will be used with input from other stakeholder groups to refine the WSIH model of care. (PsycInfo Database Record (c) 2021 APA, all rights reserved)
AD  - Templeman, Kate, NICM Health Research Institute, Western Sydney University, Locked Bag 1797, Penrith, NSW, Australia, 2751
AN  - 2020-26529-009
AU  - Ee, Carolyn
AU  - Templeman, Kate
AU  - Grant, Suzanne
AU  - Avard, Nicole
AU  - de Manincor, Michael
AU  - Reath, Jennifer
AU  - Hunter, Jennifer
DB  - psyh
DO  - 10.1089/acm.2019.0321
DP  - EBSCOhost
IS  - 4
KW  - academic health center
traditional
complementary
and integrative medicine
integrative medicine
integrative health care
evidence-based medicine
primary care
stakeholder engagement
community-based participatory research
Academic Medical Centers
Australia
Complementary Therapies
Female
General Practitioners
Health Knowledge, Attitudes, Practice
Humans
Male
Primary Health Care
Qualitative Research
Health Promotion
Integrated Services
Interviews
Stakeholder
Auditory Stimulation
Community Psychology
Evidence Based Practice
N1  - NICM Health Research Institute, Western Sydney University, Penrith, NSW, Australia. Release Date: 20210527. Publication Type: Journal (0100), Peer Reviewed Journal (0110). Format Covered: Electronic. Document Type: Journal Article. Language: EnglishMajor Descriptor: General Practitioners; Health Promotion; Integrated Services; Interviews; Stakeholder. Minor Descriptor: Auditory Stimulation; Community Psychology; Evidence Based Practice. Classification: Promotion & Maintenance of Health & Wellness (3365). Population: Human (10); Male (30); Female (40). Location: Australia. Age Group: Adulthood (18 yrs & older) (300). Methodology: Empirical Study; Interview; Qualitative Study. Page Count: 16. Issue Publication Date: Apr, 2020. Copyright Statement: Mary Ann Liebert, Inc.
Sponsor: Jacka Foundation of Natural Therapies. Other Details: to support development of an Integrative Medicine Program at NICM Health Research Institute. Recipients: No recipient indicated
PY  - 2020
SN  - 1075-5535
1557-7708
SP  - 300-315
ST  - Informing the model of care for an academic integrative health care center: A qualitative study of primary care stakeholder views
T2  - The Journal of Alternative and Complementary Medicine
TI  - Informing the model of care for an academic integrative health care center: A qualitative study of primary care stakeholder views
UR  - https://search.ebscohost.com/login.aspx?direct=true&db=psyh&AN=2020-26529-009&site=ehost-live
ORCID: 0000-0003-0612-4891
ORCID: 0000-0002-5159-4705
ORCID: 0000-0002-3363-9199
k.templeman@westernsydney.edu.au
VL  - 26
ID  - 1059
ER  - 
TY  - JOUR
AB  - Dementia and hearing loss share radiologic and biologic findings that might explain their coexistence, especially in the elderly population. Brain atrophy has been observed in both conditions, as well as the presence of areas of gliosis. The brain atrophy is usually focal; it is located in the temporal lobe in patients with hearing loss, while it involves different part of brain in patients with dementia. Radiological studies have shown white matter hyperintensities (WMHs) in both conditions. WMHs have been correlated with the inability to correctly understand words in elderly persons with normal auditory thresholds and, the identification of these lesion in brain magnetic resonance imaging studies has been linked with an increased risk of developing cognitive loss. In addition to WMHs, some anatomopathological studies identified the presence of brain gliosis in the elderly’s brain. The cause-effect link between hearing loss and dementia is still unknown, despite they might share some common findings. The aim of this systematic review is to analyze radiologic and biomolecular findings that these two conditions might share, identify a common pathological basis, and discuss the effects of hearing aids on prevention and treatment of cognitive decline in elderly patients with hearing loss. (PsycInfo Database Record (c) 2021 APA, all rights reserved)
AD  - Di Stadio, Arianna
AN  - 2021-13123-022
AU  - Di Stadio, Arianna
AU  - Ralli, Massimo
AU  - Roccamatisi, Dalila
AU  - Scarpa, Alfonso
AU  - della Volpe, Antonio
AU  - Cassandro, Claudia
AU  - Ricci, Giampietro
AU  - Greco, Antonio
AU  - Bernitsas, Evanthia
DB  - psyh
DO  - 10.1007/s10072-020-04948-8
DP  - EBSCOhost
IS  - 2
KW  - Cognitive decline
Hearing loss
Brain atrophy
White matter lesion
Cerebral Atrophy
Cognitive Impairment
Comorbidity
Dementia
Partially Hearing Impaired
AIDS Prevention
Auditory Acuity
Hearing Disorders
Brain Lesions (Disorders)
White Matter
Gliosis
N1  - Otolaryngology Department, University of Perugia, Perugia, Italy. Release Date: 20210304. Publication Type: Journal (0100), Peer Reviewed Journal (0110). Format Covered: Electronic. Document Type: Journal Article. Language: EnglishMajor Descriptor: Cerebral Atrophy; Cognitive Impairment; Comorbidity; Dementia; Partially Hearing Impaired. Minor Descriptor: AIDS Prevention; Auditory Acuity; Hearing Disorders; Brain Lesions (Disorders); White Matter; Gliosis. Classification: Physical & Somatoform & Psychogenic Disorders (3290). Population: Human (10). Methodology: Literature Review; Systematic Review. Page Count: 10. Issue Publication Date: Feb, 2021. Publication History: First Posted Date: Jan 7, 2021; Accepted Date: Nov 27, 2020; First Submitted Date: Apr 10, 2020. Copyright Statement: Fondazione Società Italiana di Neurologia. 2021.
PY  - 2021
SN  - 1590-1874
1590-3478
SP  - 579-588
ST  - Hearing loss and dementia: Radiologic and biomolecular basis of their shared characteristics A systematic review
T2  - Neurological Sciences
TI  - Hearing loss and dementia: Radiologic and biomolecular basis of their shared characteristics A systematic review
UR  - https://search.ebscohost.com/login.aspx?direct=true&db=psyh&AN=2021-13123-022&site=ehost-live
ORCID: 0000-0001-5510-3814
ariannadistadio@hotmail.com
VL  - 42
ID  - 1056
ER  - 
TY  - JOUR
AB  - Aim: The present study focuses on the implementation and evaluation of the organizational intervention 'Burnout Prevention Team' (BPT). The BPT relies on a standardized procedure and is theoretically based on the Areas of Worklife—an emprical framework of well-established work-related factors that evidently induce job burnout. Subjects and methods: To evaluate BPT, the process of intervention implementation was examined drawing on an evidence-based model of process evaluation with the focus on inititation, activities, and implementation strategies. Results: BPT was conducted in nine health care institutions. Results emphasize the relevance of ensuring management support as well as developing a sound communication and information strategy within the initiation phase. Regarding intervention activities, providing employees with knowledge about the burnout concept and trigger factors turned out to be an essential prerequisite for developing successful intervention solutions. Overall, in each institution 7 to 12 solutions to institution-specific problems were developed. Approximately 1 year after the solutions were launched at least 70% were partially or completely implemented. Conclusion: The general high implementation rate suggests that the BPT can be proposed as a successful example for an organizational-focused approach that should be highly exportable to other health care institutions. (PsycInfo Database Record (c) 2020 APA, all rights reserved)
AD  - Buruck, Gabriele
AN  - 2019-68604-007
AU  - Buruck, Gabriele
AU  - Tomaschek, Anne
AU  - Lütke-Lanfer, Sarah S.
DB  - psyh
DO  - 10.1007/s10389-018-0999-0
DP  - EBSCOhost
IS  - 6
KW  - Burnout prevention
Organizational health intervention
Qualitative process evaluation
Health care
Intervention
Occupational Stress
Prevention
Workplace Intervention
Auditory Stimulation
Communication
Occupations
Teams
N1  - Westsachsische Hochschule Zwickau, University of Applied Sciences, Zwickau, Germany. Release Date: 20201231. Publication Type: Journal (0100), Peer Reviewed Journal (0110). Format Covered: Electronic. Document Type: Journal Article. Language: EnglishMajor Descriptor: Intervention; Occupational Stress; Prevention; Workplace Intervention. Minor Descriptor: Auditory Stimulation; Communication; Occupations; Teams. Classification: Industrial & Organizational Psychology (3600). Population: Human (10); Male (30); Female (40). Age Group: Adulthood (18 yrs & older) (300); Young Adulthood (18-29 yrs) (320); Thirties (30-39 yrs) (340); Middle Age (40-64 yrs) (360). Tests & Measures: Areas of Worklife Scale-German Version; Maslach Burnout Inventory-General Survey; WHO Well-Being Index 5; Feedback Questionnaire. Methodology: Empirical Study; Quantitative Study. Supplemental Data: Tables and Figures Internet. Page Count: 12. Issue Publication Date: Dec, 2019. Publication History: First Posted Date: Nov 20, 2018; Accepted Date: Nov 6, 2018; First Submitted Date: Aug 5, 2018. Copyright Statement: Springer-Verlag GmbH Germany, part of Springer Nature. 2018.
Sponsor: AOK Plus, Gesundheitskasse für Sachsen und Thüringen. Recipients: No recipient indicated
Sponsor: Berufsgenossenschaft für Gesundheitsdienst und Wohlfahrtspflege, Germany. Recipients: No recipient indicated
PY  - 2019
SN  - 0943-1853
1613-2238
SP  - 743-754
ST  - Burnout prevention team-process evaluation of an organizational health intervention
T2  - Journal of Public Health
TI  - Burnout prevention team-process evaluation of an organizational health intervention
UR  - https://search.ebscohost.com/login.aspx?direct=true&db=psyh&AN=2019-68604-007&site=ehost-live
Gabriele.Buruck@fh-zwickau.de
VL  - 27
ID  - 1061
ER  - 
