TY  - JOUR
TI  - Ambient and at-the-ear occupational noise exposure and serum lipid levels
AU  - Arlien-Søborg, Mai C
AU  - Schmedes, Astrid S
AU  - Stokholm, Z A
AU  - Grynderup, M B
AU  - Bonde, J P
AU  - Jensen, C S
AU  - Hansen, Å M
AU  - Frederiksen, T W
AU  - Kristiansen, J
AU  - Christensen, K L
AU  - Vestergaard, J M
AU  - Lund, S P
AU  - Kolstad, H A
KW  - /dk/atira/pure/researchoutput/pubmedpublicationtype/D016428 ; Journal Article
UR  - https://curis.ku.dk/portal/da/publications/ambient-and-attheear-occupational-noise-exposure-and-serum-lipid-levels(6aab209f-5303-4de2-a379-f30fe8ff52c7).html
LA  - eng
PY  - 2016
AB  - OBJECTIVES: Occupational and residential noise exposure has been related to increased risk of cardiovascular disease. Alteration of serum lipid levels has been proposed as a possible causal pathway. The objective of this study was to investigate the relation between ambient and at-the-ear occupational noise exposure and serum levels of total cholesterol, low-density lipoprotein-cholesterol, high-density lipoprotein-cholesterol, and triglycerides when accounting for well-established predictors of lipid levels. METHODS: This cross-sectional study included 424 industrial workers and 84 financial workers to obtain contrast in noise exposure levels. They provided a serum sample and wore portable dosimeters that every 5-s recorded ambient noise exposure levels during a 24-h period. We extracted measurements obtained during work and calculated the full-shift mean ambient noise level. For 331 workers who kept a diary on the use of a hearing protection device (HPD), we subtracted 10 dB from every noise recording obtained during HPD use and estimated the mean full-shift noise exposure level at the ear. RESULTS: Mean ambient noise level was 79.9 dB (A) [range 55.0-98.9] and the mean estimated level at the ear 77.8 dB (A) [range 55.0-94.2]. Ambient and at-the-ear noise levels were strongly associated with increasing levels of triglycerides, cholesterol-HDL ratio, and decreasing levels of HDL-cholesterol, but only in unadjusted analyses that did not account for HPD use and other risk factors. CONCLUSION: No associations between ambient or at-the-ear occupational noise exposure and serum lipid levels were observed. This indicates that a causal pathway between occupational and residential noise exposure and cardiovascular disease does not include alteration of lipid levels.
ER  - 
TY  - JOUR
TI  - Ambient and at-the-ear occupational noise exposure and serum lipid levels
AU  - Arlien-Søborg, Mai C.
AU  - Schmedes, Astrid S.
AU  - Stokholm, Z. A.
AU  - Grynderup, M. B.
AU  - Bonde, J. P.
AU  - Jensen, C. S.
AU  - Hansen, M.
AU  - Frederiksen, T. W.
AU  - Kristiansen, J.
AU  - Christensen, K. L.
AU  - Vestergaard, J. M.
AU  - Lund, S. P.
AU  - Kolstad, H. A.
KW  - Cardiovascular disease ; Causal pathways ; Hearing protective devices ; Manufacturing industries
UR  - https://pure.au.dk/portal/da/publications/ambient-and-attheear-occupational-noise-exposure-and-serum-lipid-levels(9c0890d7-dede-4db2-9672-e4ffcc61dfff).html
LA  - eng
PY  - 2016
AB  - Objectives: Occupational and residential noise exposure has been related to increased risk of cardiovascular disease. Alteration of serum lipid levels has been proposed as a possible causal pathway. The objective of this study was to investigate the relation between ambient and at-the-ear occupational noise exposure and serum levels of total cholesterol, low-density lipoprotein–cholesterol, high-density lipoprotein–cholesterol, and triglycerides when accounting for well-established predictors of lipid levels. Methods: This cross-sectional study included 424 industrial workers and 84 financial workers to obtain contrast in noise exposure levels. They provided a serum sample and wore portable dosimeters that every 5-s recorded ambient noise exposure levels during a 24-h period. We extracted measurements obtained during work and calculated the full-shift mean ambient noise level. For 331 workers who kept a diary on the use of a hearing protection device (HPD), we subtracted 10 dB from every noise recording obtained during HPD use and estimated the mean full-shift noise exposure level at the ear. Results: Mean ambient noise level was 79.9 dB (A) [range 55.0–98.9] and the mean estimated level at the ear 77.8 dB (A) [range 55.0–94.2]. Ambient and at-the-ear noise levels were strongly associated with increasing levels of triglycerides, cholesterol–HDL ratio, and decreasing levels of HDL–cholesterol, but only in unadjusted analyses that did not account for HPD use and other risk factors. Conclusion: No associations between ambient or at-the-ear occupational noise exposure and serum lipid levels were observed. This indicates that a causal pathway between occupational and residential noise exposure and cardiovascular disease does not include alteration of lipid levels.
ER  - 
TY  - JOUR
TI  - Predictive factors of occupational noise-induced hearing loss in Spanish workers: A prospective study
AU  - Armando Carballo Pelegrin
AU  - Leonides Canuet
AU  - Ángeles Arias Rodríguez
AU  - Maria Pilar Arévalo Morales
KW  - Audiometry ; hearing loss ; hearing protection devices ; occupational noise ; predictors ; Otorhinolaryngology ; RF1-547 ; Industrial medicine. Industrial hygiene ; RC963-969
PB  - Wolters Kluwer Medknow Publications
UR  - https://doi.org/10.4103/1463-1741.165064
LA  - EN
PY  - 2015
AB  - The purpose of our study was to identify the main factors associated with objective noise-induced hearing loss (NIHL), as indicated by abnormal audiometric testing, in Spanish workers exposed to occupational noise in the construction industry. We carried out a prospective study in Tenerife, Spain, using 150 employees exposed to occupational noise and 150 age-matched controls who were not working in noisy environments. The variables analyzed included sociodemographic data, noise-related factors, types of hearing protection, self-report hearing loss, and auditory-related symptoms (e.g., tinnitus, vertigo). Workers with pathological audiograms had significantly longer noise-exposure duration (16.2 ± 11.4 years) relative to those with normal audiograms (10.2 ± 7.0 years; t = 3.99, P < 0.001). The vast majority of those who never used hearing protection measures had audiometric abnormalities (94.1%). Additionally, workers using at least one of the protection devices (earplugs or earmuffs) had significantly more audiometric abnormalities than those using both protection measures simultaneously (Chi square = 16.07; P < 0.001). The logistic regression analysis indicates that the use of hearing protection measures [odds ratio (OR) = 12.30, confidence interval (CI) = 4.36-13.81, P < 0.001], and noise-exposure duration (OR = 1.35, CI = 1.08-1.99, P = 0.040) are significant predictors of NIHL. This regression model correctly predicted 78.2% of individuals with pathological audiograms. The combined use of hearing protection measures, in particular earplugs and earmuffs, associates with a lower rate of audiometric abnormalities in subjects with high occupational noise exposure. The use of hearing protection measures at work and noise-exposure duration are best predictive factors of NIHL. Auditory-related symptoms and self-report hearing loss do not represent good indicators of objective NIHL. Routine monitoring of noise levels and hearing status are of great importance as part of effective hearing conservation programs.
ER  - 
TY  - JOUR
TI  - PENGARUH PENYULUHAN TERHADAP PENGETAHUAN, SIKAP, DAN TINDAKAN MAHASISWA TERKAIT PENGGUNAANALAT PELINDUNG TELINGA DARI BAHAYA KEBISINGAN SAAT MENGGERINDA DI RUANG PENGELASAN UNIVERSITAS NEGERI MALANG
AU  - Baihaq, Firda
AU  - Marji, Marji
AU  - JOUR
PB  - Universitas Negeri Malang
UR  - http://journal2.um.ac.id/index.php/preventia/article/view/2747
LA  - eng
PY  - 2016
AB  - Abstract: The noise level from grinding activities in Welding Laboratory State University of Malang is 98 dB exceeds the Threshold Limit Value (TLV) is 85 dB. Students are not always use provided ear protective equipment which can cause occupational diseases such as Noise Induced Hearing Loss. According to the World Health Organization, counseling is one of health promotion technique to prevent disease and provide health information. The variables which measured are knowledge, attitude, and practice in using Ear Protective Equipment. This research use one group pretest posttest method design with 17 students at Welding Laboratory State University of Malang. Counseling was held by presentation and handout materials which followed with lecture and discussion. Data analys with Wilcoxon Signed Ranks Test, showed that: the value of Z -3.644 and Asymp. Sig. (2-tailed) of 0.000 at knowledge variable, the value of Z -3.368 and Asymp. Sig. (2-tailde) 0.001 on attitude variable, and the value of Z -3873 and Asymp. Sig. (2-tailed) 0.001 on practice variable is smaller than 0.05. The conclusion of hypothesis is that there is any effect of counseling to student knowledge, attitude, and practice in using Ear Protective Equipment from grinding noise hazard at Welding Laboratory State University of Malang.Keywords: counseling, knowledge, attitude, practice, ear protective equipmentAbstrak: Kebisingan yang ditimbulkan dari kegiatan menggerinda di Ruang Pengelasan Universitas Negeri Malang adalah 98 dB melebihi Nilai Ambang Batas (NAB) yaitu 85 dB. Mahasiswa saat menggerinda tidak selalu menggunakan Alat Pelindung Telinga sehingga dapat menyebabkan penyakit akibat kerja berupa Noise Induced Hearing Loss. Menurut World Health Organization, penyuluhan merupakan salah satu cara promosi kesehatan untuk mencegah penyakit dan bertujuan memberikan informasi kesehatan. Variabel yang diukur dalam penelitian ini yaitu pengetahuan, sikap, dan tindakan mahasiswa terkait penggunaan Alat Pelindung Telinga. Metode penelitian ini menggunakan desain penelitian one group pretest posttest terhadap 17 mahasiswa di Ruang Pengelasan Universitas Negeri Malang. Penyuluhan dilakukan dengan media presentasi dan handout materi serta metode ceramah dan tanya jawab. Analisis data menggunakan Wilcoxon Signed Ranks Test menunjukkan hasil: nilai Z -3,644 dan Asymp. Sig. (2-tailed) 0,000 pada variabel pengetahuan, nilai Z -3,368 dan Asymp. Sig. (2-tailed) 0,001 pada variabel sikap, serta nilai Z -3873 dan Asymp. Sig. (2-tailed) 0,001 pada variabel tindakan mahasiswa berarti lebih kecil daripada 0,05. Kesimpulan dari pengujian hipotesis tersebut adalah terdapat pengaruh penyuluhan terhadap pengetahuan, sikap, dan tindakan mahasiswa terkait penggunaan Alat Pelindung Telinga dari bahaya kebisingan saat menggerinda di Ruang Pengelasan Universitas Negeri Malang.Kata kunci: penyuluhan, pengetahuan, sikap, tindakan, alat pelindung telinga
ER  - 
TY  - ADVS
TI  - Attitudes toward Noise, Perceived Hearing Symptoms, and Reported Use of Hearing Protection among College Students: Influence of Youth Culture
AU  - Balanay, Jo Anne
AU  - Kearney, Gregory D.
KW  - Hearing Protection ; Youth Culture ; Noise-induced hearing loss (NIHL)
UR  - http://hdl.handle.net/10342/5969
LA  - en_US
PY  - 2016
AB  - Presented for World Environmental Health Day, September 26, 2016 in Greenville, North Carolina. ; Background â€¢ Young adults are involved in noisy activities, increasing their risk of developing noise-induced hearing loss (NIHL) and other hearing symptoms. â€¢ NIHL and other hearing symptoms are increasing in the younger population in the U.S. and abroad. Methodology â€¢ A 44-item online survey was administered to students enrolled in a personal health course (HLTH 1000) through Qualtrics. â€¢ 2,151 college students participated (92.3% participation rate) â€¢ Survey instrument includes: o Demographic items o Youth attitudes to noise scale (YANS) o Hearing symptom description (HSD) o Noise exposure and hearing protection use (AAH) Conclusions â€¢ Universities/ colleges have important roles in protecting young adultsâ€™ hearing by: o Integrating hearing conservation topic in the college curriculum o Promoting hearing health by student health services o Involving student groups in NIHL awareness and prevention o Establishing noise level limitations for all on-campus events Jo Anne G. Balanay, PhD, CIH1 and Gregory D. Kearney, MPH, DrPH2 1Environmental Health Sciences Program, Department of Health Education and Promotion, College of Health and Human Performance, East Carolina University 2Department of Public Health, Brody School of Medicine, East Carolina University Purpose of the Study â€¢ To assess the attitude toward noise, perceived hearing symptoms, noisy activities that were participated in, and factors associated with hearing protection use among college students â€¢ Goal: To understand the risk factors influencing the behavior of college students related to noise exposure to effectively promote and implement hearing conservation programs for them.
ER  - 
TY  - JOUR
TI  - Evidence and consensus based guideline for the management of delirium, analgesia, and sedation in intensive care medicine. Revision 2015 (DAS-Guideline 2015) - short version ; S3-Leitlinie Analgesie, Sedierung und Delirmanagement in der Intensivmedizin. Revision 2015 (DAS-Leitlinie 2015) - Kurzversion
AU  - Baron, R
AU  - Binder, A
AU  - Biniek, R
AU  - Braune, S
AU  - Buerkle, H
AU  - Dall, P
AU  - Demirakca, S
AU  - Eckardt, R
AU  - Eggers, V
AU  - Eichler, I
AU  - Fietze, I
AU  - Freys, S
AU  - Fründ, A
AU  - Garten, L
AU  - Gohrbandt, B
AU  - Harth, I
AU  - Hartl, W
AU  - Heppner, HJ
AU  - Horter, J
AU  - Huth, R
AU  - Janssens, U
AU  - Jungk, C
AU  - Kaeuper, KM
AU  - Kessler, P
AU  - Kleinschmidt, S
AU  - Kochanek, M
AU  - Kumpf, M
AU  - Meiser, A
AU  - Mueller, A
AU  - Orth, M
AU  - Putensen, C
AU  - Roth, B
AU  - Schaefer, M
AU  - Schaefers, R
AU  - Schellongowski, P
AU  - Schindler, M
AU  - Schmitt, R
AU  - Scholz, J
AU  - Schroeder, S
AU  - Schwarzmann, G
AU  - Spies, C
AU  - Stingele, R
AU  - Tonner, P
AU  - Trieschmann, U
AU  - Tryba, M
AU  - Wappler, F
AU  - Waydhas, C
AU  - Weiss, B
AU  - Weisshaar, G
AU  - DAS-Taskforce 2015
KW  - guideline ; evidence ; analgesia ; sedation ; delirium ; anxiety ; stress ; sleep ; monitoring ; treatment ; intensive care ; critical care ; Germany ; Leitlinie ; Analgesie ; Sedierung ; Delir ; Angst ; Schlaf ; Therapie ; Intensivmedizin ; ddc: 610
PB  - German Medical Science GMS Publishing House; Düsseldorf
UR  - https://doi.org/10.3205/000223
LA  - eng
PY  - 2015
AB  - In 2010, under the guidance of the DGAI (German Society of Anaesthesiology and Intensive Care Medicine) and DIVI (German Interdisciplinary Association for Intensive Care and Emergency Medicine), twelve German medical societies published the "Evidence- and Consensus-based Guidelines on the Management of Analgesia, Sedation and Delirium in Intensive Care". Since then, several new studies and publications have considerably increased the body of evidence, including the new recommendations from the American College of Critical Care Medicine (ACCM) in conjunction with Society of Critical Care Medicine (SCCM) and American Society of Health-System Pharmacists (ASHP) from 2013. For this update, a major restructuring and extension of the guidelines were needed in order to cover new aspects of treatment, such as sleep and anxiety management. The literature was systematically searched and evaluated using the criteria of the Oxford Center of Evidence Based Medicine. The body of evidence used to formulate these recommendations was reviewed and approved by representatives of 17 national societies. Three grades of recommendation were used as follows: Grade "A" (strong recommendation), Grade "B" (recommendation) and Grade "0" (open recommendation). The result is a comprehensive, interdisciplinary, evidence and consensus-based set of level 3 guidelines. This publication was designed for all ICU professionals, and takes into account all critically ill patient populations. It represents a guide to symptom-oriented prevention, diagnosis, and treatment of delirium, anxiety, stress, and protocol-based analgesia, sedation, and sleep-management in intensive care medicine. ; Die vorherige Version der S3-Leitlinie "Analgesie, Sedierung und Delirmanagement in der Intensivmedizin" wurde 2010 unter der Federführung der Deutschen Gesellschaft für Anästhesiologie und Intensivmedizin (DGAI) und der Deutschen Interdisziplinären Vereinigung für Intensiv- und Notfallmedizin (DIVI) publiziert. Neue Evidenz aus Studien ebenso wie neue Leitlinien, u.a. die 2013 erschienene Leitlinie der U.S.-amerikanischen Society of Critical Care Medicine (SCCM), des American College of Critical Care Medicine (ACCM) und der American Society of Health-System Pharmacists (ASHP), gaben nicht nur Anlass zu einem Update der deutschen Empfehlungen von 2010. Für die Fortschreibung der S3-Leitlinie wurden eine Neuformulierung von klinisch relevanten Schlüsselfragen und die signifikante Erweiterung der Leitlinie um neue Facetten der Behandlung, wie zum Beispiel das Schlafmanagement, notwendig. Dazu wurde die systematisch gesuchte Literatur nach Kriterien des Oxford Centre of Evidence Based Medicine bewertet. Der enorme Evidenzkörper bildete die Grundlage für die Empfehlungen, die von Mandatsträgern aus 17 Fachgesellschaften konsentiert wurden. In den Empfehlungen wurden die Grade "A" (starke Empfehlung), "B" (Empfehlung) und "0" (offene Empfehlung) gewählt. Als Ergebnis dieses Prozesses liegt nun die weltweit umfassendste, interdisziplinär erarbeitete evidenz- und konsensbasierte Stufe 3 Leitlinie vor. Die Leitlinie richtet sich an alle auf der Intensivstation tätigen Berufsgruppen, die Empfehlungen berücksichtigen alle intensivmedizinisch-behandelten Patientengruppen. Sie stellt einen Leitfaden zur symptomorientierten Prävention, Diagnostik und Therapie von Delir, Angst, Stress und der protokollbasierten Analgesie, Sedierung und dem Schlafmanagement in der Intensivmedizin für Erwachsene und Kinder dar.
ER  - 
TY  - JOUR
TI  - A Qualitative study of earplug use as a health behavior : the role of noise injury symptoms, self-efficacy and an affinity for music
AU  - Beach, E. F
AU  - Williams, W
AU  - Gilliver, M
KW  - earplugs ; health belief model ; leisure noise ; loud music ; nightclubs
PB  - Sage Publications
UR  - http://hdl.handle.net/1959.14/1220373
LA  - eng
PY  - 2012
AB  - The use of earplugs in loud music venues is confined to a small minority who wish to avoid hearing damage from excessive noise exposure. Using the framework of the health belief model (HBM), structured interviews were held with 20 earplug-wearing clubbers. Qualitative analysis revealed the HBM constructs relevant to understanding this group’s motivation to protect their hearing. Personal experience of noise injury symptoms was the most common cue triggering earplug use. Awareness of the benefits of earplugs and appreciation of the long-term implications of hearing damage, affinity for music and high self-efficacy were also key variables underlying this health behaviour. ; 10 page(s)
ER  - 
TY  - JOUR
TI  - Hearing protection devices: Use at work predicts use at play
AU  - Beach, Elizabeth Francis
AU  - Gilliver, Megan
AU  - Williams, Warwick
KW  - Toxicology ; Public Health, Environmental and Occupational Health ; Health, Toxicology and Mutagenesis ; General Environmental Science
PB  - Informa UK Limited
UR  - http://dx.doi.org/10.1080/19338244.2015.1089828
LA  - en
PY  - 2016
ER  - 
TY  - JOUR
TI  - Hearing protection devices : use at work predicts use at play
AU  - Beach, Elizabeth Francis
AU  - Gilliver, Megan
AU  - Williams, Warwick
KW  - citizen science ; hearing protection ; hearing protection devices ; leisure noise ; workplace health and safety
PB  - Taylor & Francis
UR  - http://hdl.handle.net/1959.14/1220352
LA  - eng
PY  - 2016
AB  - Use of hearing protection devices (HPDs) at work is widespread and well researched, but less is known about HPD use in high-noise leisure activities. We investigated HPD use of 8,144 Australians in leisure settings. An online survey asked questions about HPD use at work and leisure and examined whether age, gender, HPD use at work, and tinnitus predicted HPD use in leisure activities. Leisure-based HPD use was most common during high-risk work-related activities. Use of HPDs at work was the most significant predictor of leisure-based use, with workplace users up to 5 times more likely to use HPDs at leisure. Men were significantly more likely than women to use HPDs in 10/20 leisure activities, and those with tinnitus were more likely than those without to use HPDs in 8/20 activities. Older participants were more likely to use HPDs at nightclubs and concerts, but younger participants were more likely to use HPDs playing e-games and musical instruments. ; 8 page(s)
ER  - 
TY  - JOUR
TI  - Providing earplugs to young adults at risk encourages protective behaviour in music venues
AU  - Beach, Elizabeth Francis
AU  - Nielsen, Lillian
AU  - Gilliver, Megan
KW  - behavioural change ; concerts ; noise-induced hearing loss ; tinnitus ; prevention ; soft paternalism ; earplugs ; health behaviour ; health promotion ; hearing loss ; loud music ; music venues ; nightclubs ; noise
PB  - SAGE Publications
UR  - http://hdl.handle.net/1959.14/1190130
LA  - eng
PY  - 2016
AB  - For some young people, nightclubs and other music venues are a major source of noise exposure, arising from a combination of very high noise levels; relatively long attendance duration; and frequent, sustained participation over several years. Responsibility for hearing protection is largely left to individuals, many of whom choose not to wear earplugs. In order to encourage earplug use in these settings, a new approach is needed. The aim of the study was to examine whether presentation of hearing health information would result in increased use of earplugs, or whether provision of earplugs alone would be sufficient to change behaviour. A total of 51 regular patrons of music venues were allocated to either a low-information (lo-info) or high-information (hi-info) group. Both groups completed a survey about their current noise exposure, earplug usage and perceived risk of hearing damage. Both groups were also provided with one-size-fits-all filtered music earplugs. The hi-info group was also provided with audio-visual and written information about the risks of excessive noise exposure. After 4 weeks, and again after an additional 12 weeks, participants were asked about their recent earplug usage, intention to use earplugs in the future, and perceived risk of hearing damage. The results showed that after 4 weeks, the hi-info group’s perceived personal risk of hearing damage was significantly higher than that of the lo-info group. After 16 weeks, these differences were no longer evident; however, at both 4 and 16 weeks, both the lo- and hi-info groups were using the earplugs equally often; and both groups intended to use earplugs significantly more often in the future. This suggests that the information was unnecessary to motivate behavioural change. Rather, the simple act of providing access to earplugs appears to have effectively encouraged young at-risk adults to increase their earplug use. ; 12 page(s)
ER  - 
