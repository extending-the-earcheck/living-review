TY  - CONF
TI  - Audibility of warning signals: methods to evaluate the combined effects of hearing protectors and hearing impairment
AU  - El Sawaf, Ossen
AU  - Arz, Jean-Pierre
AU  - Grimault, Nicolas
AU  - Chevret, Patrick
KW  - Noise ; Environment ; Annoyance I-INCE Classification of Subject Number: 36 ; 61 ; 76 ; 79 ; [PHYS.MECA.ACOU]Physics [physics]/Mechanics [physics]/Acoustics [physics.class-ph]
PB  - HAL CCSD
UR  - https://hal.archives-ouvertes.fr/hal-02331722
LA  - en
PY  - 2019
AB  - International audience ; Workers exposed to high noise levels are generally required to wear hearing protection devices (HPDs). For security reasons, it is important to evaluate whether the audibility of warning signals in noise is impeded when using HPDs, especially for hearing-impaired workers. The typical method to assess the effect of HPDs is to perform subjective experiments which require large populations with various degrees of hearing impairment to get statistically significant results. To ease the evaluation of the combined effects of HPDs and hearing impairment, two alternative methods are proposed. First, sound simulations have been used to reproduce the combined effects of impairment and HPDs to perform listening tests on normal hearing listeners. Qualitatively, the effects are successfully simulated. Second, a detection model has been developed to predict masked thresholds of warning signals in presence of background noise. Even though it overestimates the detrimental effects of HPDs, the model is qualitatively in agreement with the experimental data. 1 ossen.elsawaf@inrs.fr 2 jean-pierre.arz@inrs.fr 3 nicolas.grimault@cnrs.fr 4 patrick.chevret@inrs.fr Overall, these results should help to produce general recommendations for security purpose at work.
ER  - 
TY  - CONF
TI  - Audibility of warning signals: methods to evaluate the combined effects of hearing protectors and hearing impairment
AU  - El Sawaf, Ossen
AU  - Arz, Jean-Pierre
AU  - Grimault, Nicolas
AU  - Chevret, Patrick
KW  - Noise ; Environment ; Annoyance I-INCE Classification of Subject Number: 36 ; 61 ; 76 ; 79 ; [PHYS.MECA.ACOU]Physics [physics]/Mechanics [physics]/Acoustics [physics.class-ph]
PB  - HAL CCSD
UR  - https://hal.archives-ouvertes.fr/hal-02331722
LA  - en
PY  - 2019
AB  - International audience ; Workers exposed to high noise levels are generally required to wear hearing protection devices (HPDs). For security reasons, it is important to evaluate whether the audibility of warning signals in noise is impeded when using HPDs, especially for hearing-impaired workers. The typical method to assess the effect of HPDs is to perform subjective experiments which require large populations with various degrees of hearing impairment to get statistically significant results. To ease the evaluation of the combined effects of HPDs and hearing impairment, two alternative methods are proposed. First, sound simulations have been used to reproduce the combined effects of impairment and HPDs to perform listening tests on normal hearing listeners. Qualitatively, the effects are successfully simulated. Second, a detection model has been developed to predict masked thresholds of warning signals in presence of background noise. Even though it overestimates the detrimental effects of HPDs, the model is qualitatively in agreement with the experimental data. 1 ossen.elsawaf@inrs.fr 2 jean-pierre.arz@inrs.fr 3 nicolas.grimault@cnrs.fr 4 patrick.chevret@inrs.fr Overall, these results should help to produce general recommendations for security purpose at work.
ER  - 
TY  - JOUR
TI  - Attenuation of ultrasonic noise in the 10–16 kHz frequency range by earplugs
AU  - Emil Kozłowski
AU  - Rafał Młyński
KW  - hearing protectors ; earplugs ; ultrasonic noise ; sound attenuation ; hearing conservation ; REAT ; Public aspects of medicine ; RA1-1270
PB  - Nofer Institute of Occupational Medicine
UR  - https://doi.org/10.13075/mp.5893.00721
LA  - EN ; PL
PY  - 2018
AB  - Background The aim of the study was to determine attenuation of earplugs for ultrasonic noise in the frequency range of 10–16 kHz. Material and Methods The attenuation of earplugs in 1/3-octave-bands with the centre frequencies of 10 kHz, 12.5 kHz, and 16 kHz using the REAT (real-ear attenuation at threshold) method based on the measurements of hearing threshold of subjects. The study was carried out for 29 models of earplugs commonly used in the industry designed by various manufacturers, including 13 models of foam earplugs, 10 models of flanged earplugs, 5 models of headband earplugs and one model of no-roll earplugs. Results The values of the measured attenuation of earplugs are in the range 12.9–33.2 dB for the 10 kHz frequency band, 22.8–35.2 dB for the 12.5 kHz frequency band and 29.5–37.2 dB for the 16 kHz frequency band. The attenuation of earplugs in the frequency range 10–16 kHz has higher values (statistically significant changes) for foam earplugs than flanged earplugs (p = 0.0003 vs. p = 0.0006) or headband earplugs (p = 0.0002 vs. p = 0.04). Conclusions The tests indicated that there is no uniform relation between the sound attenuation in the frequencies range of 10–16 kH and the catalogue H parameter (high-frequency attenuation value) of earplugs. Therefore, it is not possible to easily predict the attenuation of ultrasonic noise in the frequency range of 10–16 kHz using the sound attenuation data for the normally considered frequency range (up to 8 kHz). Med Pr 2018;69(4):395–402
ER  - 
TY  - JOUR
TI  - Percepción del riesgo en el ámbito laboral
AU  - Fajardo-Zapata, Álvaro Luis
AU  - González Valencia, Yuri Lilian
AU  - Hernández Niño, Jenny Fabiola
AU  - Torres Pérez, Myriam Leonor
AU  - Hernández, Héctor Andrés
KW  - Gestión de Riesgos; Riesgos Laborales; Factores de Riesgo; Percepción Social y Accidentes de Trabajo
PB  - Sello Editorial UNAD
UR  - http://hemeroteca.unad.edu.co/index.php/wp_ecisa/article/view/3203
LA  - spa
PY  - 2019
AB  - Introducción: Se presenta el tema de la percepción del riesgo como un elemento pertinente en el ámbito laboral para la prevención de Accidentes de Trabajo y Enfermedad Laboral.Desarrollo: Existen factores y teorías relacionadas que tratan de abordar este tema y dar una explicación coherente con el actuar humano; se mencionan las características estructurales o institucionales, los aspectos individuales y los psicológicos. En lo estructural se destaca, la cultura organizacional hacia la seguridad, el cumplimiento, la confianza y el compromiso de las organizaciones hacia la seguridad de los trabajadores. En lo individual, se plantea que la presión ejercida por los compañeros en el sitio de trabajo, influye en cómo los trabajadores perciben y asumen el riesgo y en lo psicológico, el nivel de conocimiento del individuo sobre el riesgo, juega un papel muy importante, ya que las personas que tienen menos información sobre éstos en los sitios de trabajo son menos propensas a correr riesgos, mientras que quienes tienen un grado mayor de conocimiento tienen la posibilidad de correr más riesgos. Se debe agregar, que la tendencia hacia el optimismo es otro de los factores que inciden en la percepción del riesgo; muchos trabajadores creen que un accidente es menos probable que les ocurra a ellos que a otras personas y subestiman los riesgos de su actividad laboral.
ER  - 
TY  - THES
TI  - Sensitivity of exposure-response relationships to exposure assessment strategies in retrospective cohort studies
AU  - THES
UR  - http://hdl.handle.net/2429/18472
LA  - eng
PY  - 2006
AB  - Introduction: Exposure misclassification in epidemiologic studies results in attenuated exposure-response relationships. Using quantitative exposure estimates helps reduce exposure misclassification in retrospective studies, but it does not eliminate it. This study focused on two areas that impact the degree of exposure misclassification inherent in quantitative exposure estimates: (1) the choice of exposure indicator where the exposure of interest is a component of a mixture; (2) the choice between expert judgment and measurement-based exposure assessment strategies. Methods: These themes were examined empirically using data from two Canadian retrospective occupational cohort studies: the BC Aluminum Smelter Cohort (n=6423) and the BC Sawmill Cohort (n=26,847). In the smelter cohort exposure to polycyclic aromatic hydrocarbons (PAH), measured as benzene soluble materials (BSM) and benzo(a)pyrene (BaP), was examined in relation to mortality and cancer incidence. In the sawmill cohort three exposures classes were evaluated: (1) nonspecific dust and wood dust in relation to chronic obstructive pulmonary disease hospitalizations; (2) total chlorophenols, pentachlorophenols (PCP), and tetrachlorophenol (TCP) in relation to cancer incidence; and (3) noise estimated using expert-based and measurement-based approaches in relation to heart disease mortality. The shape, goodness of fit, and precision of the exposure-response relationships were evaluated using Poisson regression. Results: Using a more specific exposure measure that was more proximal to the causal agent(s) improved the precision of the exposure-response relationship by between 1--14% for BaP over BSM, by 10--30% for PCP over total chlorophenols, and by 218% for wood dust over nonspecific dust. Measurement-based noise estimates improved the precision by 12--108% over the use of expert ratings. Accounting for hearing protection use in the measurement-based noise estimates improved the precision by 58% over the unadjusted estimates. The observed attenuation was not correlated with predicted attenuation from theoretical equations. Conclusions: Examining the precision of exposure-response relationships provided a quantifiable measure for evaluating different exposure assessment approaches. Refining the quantitative exposure estimates, through the use of more proximal exposure measures and the use of exposure measurements, resulted in stronger, more precise exposure-response relationships. With the exception of the expert-based noise estimates, the more common, less specific exposure measures resulted in inconclusive exposure-disease associations. ; Medicine, Faculty of ; Population and Public Health (SPPH), School of ; Graduate
ER  - 
TY  - THES
TI  - Hearing Protection Use and Intertemporal Choice in Industrial Workers
AU  - THES
UR  - http://d-scholarship.pitt.edu/37157/
LA  - en
PY  - 2019
AB  - Intertemporal choices represent scenarios where costs and benefits occur at different times, during which individuals tend to devalue larger, future rewards in favor of immediate rewards of less value. The degree to which future rewards lose their value is reflected by their temporal discount rate. The relationship between temporal discounting and behavior has been evaluated in a variety of healthcare studies, with many sources reporting a significant relationship between temporal discounting and unhealthy behaviors. The role of discounting has not been applied to understanding hearing protection device (HPD) habits of industrial workers who are overexposed to occupational noise. The purpose of this study is to investigate the relationship between discounting and HPD compliance for industrial workers. We also examine whether self-efficacy is related to compliance. This study applies a self-administered survey instrument to assess demographics, protective behavior, self-efficacy, and limited health information. Discount rates are elicited using a tool that presents a series of hypothetical monetary choices as a proxy. A logistic regression model was used to analyze whether there is a predictive relationship between temporal discount rates and HPD compliance. The collective contributions of discounting, self-efficacy, and demographics were concurrently analyzed in the model. We found no evidence of a relationship between discount rates and HPD behavior. Self-efficacy and gender were significant predictors of compliance, which has been reported previously. Our findings also provide support for broadening research inclusion criteria when studying worker populations in HPD studies. This study provides the basis for future work investigating the relationship between temporal discounting and HPD compliance.
ER  - 
TY  - JOUR
TI  - The analysis of professional conduct of occupational physicians regarding workers with hearing loss ; La analice da conducta profesional de mádicos ocupacionales con respecto a trabajadores con pérdida auditiva ; Análise da conduta de médicos do trabalho diante de trabalhadores com perda auditiva
AU  - Gatto, Clasdi I.
AU  - Lermen, Rose A.
AU  - Teixeira, Tatiane M.
AU  - Magni, Cristiana
AU  - Morata, Thaís C.
KW  - Hearing conservation; occupational medicine; auditory evaluation ; conservación auditiva; medicina ocupacional; evaluación auditiva ; conservação auditiva; medicina ocupacional; avaliação auditiva
PB  - Pontifícia Universidade Católica de São Paulo
UR  - http://revistas.pucsp.br/index.php/dic/article/view/11686
LA  - por
PY  - 2012
AB  - The aim of this study was to identify physicians’ conduct and attitude when facing work-related hearing loss among noise-exposed. Occupational physicians from the south region of Brazil were interviewed. The main factors considered in the determination of the aptitude to work related to hearing were the kind and the degree of hearing disorder, and the job function that the worker will perform. When hearing losses were detected in the pre-employment exam, the most common measure taken was to recommend a more frequent audiologic examination and orientation on the use of the hearing protection devices. Regarding Hearing Loss Prevention Programs, the participants emphasized the importance of training programs and the collaboration between a qualified multiprofissional team. ; El presente estudio consiste de entrevistas con médicos ocupacionales de la región sur del país, con el objetivo de identificar su visión y conductas respeto a la salud auditiva de trabajadores expuestos a niveles elevados de presión sonora, susceptibles a daños auditivos. Los principales factores tomados en consideración en la determinación de la aptitud en relación a la audición fueron el tipo y grado de alteración auditiva y la función que el trabajador iría a ejercer. En el caso de liminares auditivos alterados en el examen admisional, la conducta mas sencilla fue el parecer de aptitud al trabajo con control audiológico mas frecuente y orientación para el uso de EPI. Respeto al Programa de Prevención de Perdidas Auditivas (PPPA), fue destacada la etapa de orientación y concientización del trabajador para prevención auditiva, así como la actuación de un equipo multiprofesional calificada con la participación efectiva de la empresa. ; O presente estudo consta de entrevistas com médicos do trabalho da Região Sul do país, com o objetivo de identificação de sua visão e de suas condutas com relação à saúde auditiva dos trabalhadores expostos a níveis elevados de pressão sonora, susceptíveis a danos auditivos. Os principais fatores levados em consideração na determinação de aptidão em relação à audição foram o tipo e grau de alteração auditiva e a função que o trabalhador irá exercer. No caso de limiares auditivos alterados no exame admissional, a conduta mais comum foi o parecer de aptidão ao trabalho com controle audiológico mais freqüente e orientação para o uso do EPI. Quanto ao que se refere ao Programa de Prevenção de Perdas Auditivas (PPPA), foi destacada a etapa de orientação e conscientização do trabalhador para prevenção auditiva, bem como a atuação de uma equipe multiprofissional qualificada com a participação efetiva da empresa.
ER  - 
TY  - JOUR
TI  - ANALISIS PENGARUH SIKAP, KONTROL PERILAKU, DAN NORMA SUBJEKTIF TERHADAP PERILAKU SAFETY
AU  - Gilang Dwi Prakoso
AU  - Mohammad Zainal Fatah
KW  - Public aspects of medicine ; RA1-1270
PB  - Universitas Airlangga
UR  - https://doi.org/10.20473/jpk.V5.I2.2017.193-204
LA  - ID
PY  - 2018
AB  - Safety behavior in the workplace has aims to make the workers get off from work accidents that can cause financial or material losses, disability and death. The workers supposed to work safety so that companies or workplaces get the financial or material benefits. The purpose of this research was to find out the factors that affect workers to work safety. This research was an analytical descriptive with quantitative approach. Data collection was using questionnaires to respondents. The worker’s safety behavior which difficult to control when working made the number of workplace accident bigger. To know the factors that influence safety behavior was by connecting attitude, subjective norm, and worker’s perceived with safety behavior according to company rules. The results of this study showed that there was a relationship between attitude, perceived, and subjective norm with the worker’s safety behavior. This is showed by the use of PPE such as helmet, shoes, and earplug. The important of safety behavior for the worker was to alert the worker of their safety and security when they were working so they not got financial and material loss for the company, disability and death of the workers. Keyword: safety, perceived, subjective norm and attitude
ER  - 
TY  - JOUR
TI  - Prevalence of leisure noise-induced tinnitus and the attitude toward noise in university students
AU  - Gilles, Annick
AU  - De Ridder, Dirk
AU  - Van Hal, Guido F.
AU  - Wouters, Kristien
AU  - Kleine Punte, Andrea
AU  - Van de Heyning, Paul
KW  - Human medicine
UR  - https://hdl.handle.net/10067/1008270151162165141
LA  - eng
PY  - 2012
AB  - Abstract: Background: Adolescents and young adults often are exposed to potentially damaging loud music during leisure activities. As a consequence, more and more young adults suffer from tinnitus, hearing loss, and hyperacusis. The present study provides prevalence numbers for noise-induced tinnitus (NIT) in this group, the attitude toward loud music, and the factors influencing the use of hearing protection (HP). Method: A questionnaire was undertaken to evaluate the influence of permanent/transient tinnitus after loud music, the attitudes toward noise, influence of peers, and the ability to manipulate HP on the use of HP. The questionnaire was completed by 145 university students. Results: Approximately 89.5% of the students had experienced transient tinnitus after loud music exposure. The prevalence of transient NIT was higher in female subjects compared with male students. Permanent NIT was experienced by 14.8%. Nevertheless, few respondents were worried, and the degree of HP use was low (11%). However, the presence of permanent tinnitus was a motivation for HP use. Most respondents held a neutral to positive attitude (i.e., pronoise) toward loud music and were not fully aware of the risks of too much noise exposure. Conclusion: NIT is a common phenomenon among young adults. The lack of knowledge in young adults and the underuse of HP in leisure activities provide useful information to refine preventive measures in the future.
ER  - 
TY  - JOUR
TI  - A little bit less would be great: Adolescents' opinion towards music levels
AU  - Gilles, Annick
AU  - Thuy
AU  - De Rycke, Els
AU  - Van de Heyning, Paul
KW  - Human medicine
UR  - https://hdl.handle.net/10067/1211000151162165141
LA  - eng
PY  - 2014
AB  - Abstract: Many music organizations are opposed to restrictive noise regulations, because of anxiety related to the possibility of a decrease in the number of adolescents attending music events. The present study consists of two research parts evaluating on one hand the youth's attitudes toward the sound levels at indoor as well as outdoor musical activities and on the other hand the effect of more strict noise regulations on the party behavior of adolescents and young adults. In the first research part, an interview was conducted during a music event at a youth club. A total of 41 young adults were questioned concerning their opinion toward the intensity levels of the music twice: Once when the sound level was 98 dB(A), L-Aeq,L- 60min and once when the sound level was increased up to 103 dB(A), L-Aeq,L- 60min. Some additional questions concerning hearing protection (HP) use and attitudes toward more strict noise regulations were asked. In the second research part, an extended version of the questionnaire, with addition of some questions concerning the reasons for using/not using HP at music events, was published online and completed by 749 young adults. During the interview, 51% considered a level of 103 dB(A), L-Aeq,L- 60min too loud compared with 12% during a level of 98 dB(A), L-Aeq,L- 60min. For the other questions, the answers were similar for both research parts. Current sound levels at music venues were often considered as too loud. More than 80% held a positive attitude toward more strict noise regulations and reported that they would not alter their party behavior when the sound levels would decrease. The main reasons given for the low use of HP were that adolescents forget to use them, consider them as uncomfortable and that they never even thought about using them. These results suggest that adolescents do not demand excessive noise levels and that more strict noise regulation would not influence party behavior of youngsters.
ER  - 
