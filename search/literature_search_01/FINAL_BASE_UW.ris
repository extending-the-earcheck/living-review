TY  - JOUR
TI  - Verifying the attenuation levels of earmuff hearing protectors in Oil & Gas workers ; Verificación de niveles de atenuación de protectores auditivos tipo copa en trabajadores del sector Hidrocarburos
AU  - Upegui-Rincon, Sildrey
AU  - Araque-Muñoz, Luis Guillermo
AU  - Lizarazo-Salcedo, Cesar German
AU  - Berrio Garcia, Shyrle
AU  - Guarguati-Ariza, Juliana Andrea
KW  - Industrial hygiene ; occupational health ; engineering ; Occupational noise ; occupational exposure ; ear protective devices ; earmuffs ; noise ; Higiene industrial ; salud ocupacional ; Ingeniería industrial ; Ruido en el ambiente de trabajo ; exposición ocupacional ; dosimetría ; equipo de protección personal ; ruido ;
PB  - Universidad Nacional de Colombia - Sede Bogotá - Facultad de Medicina - Instituto de Salud Pública
UR  - https://revistas.unal.edu.co/index.php/revsaludpublica/article/view/70989
LA  - spa
PY  - 2019
AB  - Objective To verify the attenuation levels of two types of earmuffs under real operating conditions.Materials and Methods A study with experimental design was carried out to obtain repeated measurements of the sound pressure levels inside and outside hearing protectors in a sample of workers of an oil and gas company working under normal process and exposure conditions. The results allowed determining differences between the attenuation levels established by the manufacturer, the adjusted attenuation levels under the NIOSH method, and the attenuation levels obtained experimentally.Results The attenuation values established by the manufacturers are lower than those ones obtained under actual use conditions in all cases evaluated. Likewise, the attenuation values of the hearing protectors, once adjusted under the NIOSH method, reached values much closer to those obtained experimentally.Conclusions The variability between theoretical attenuation values and experimental values allows concluding that the attenuation levels obtained under controlled laboratory conditions do not take into account certain characteristics that, based on their use, affect the efficiency of the hearing protection device. This study encourages the implementation of comprehensive hearing protection programs that consider variables such as hearing protection effectiveness, under real use conditions, by applying fit tests or other adjustment factors like the one suggested by NIOSH. This would ensure an adequate selection that aims at achieving an effective control of this risk factor. ; Objetivo Verificar los niveles de atenuación de dos tipos de protectores auditivos de copa bajo condiciones reales de operación.Métodos Se realizó un diseño experimental de medidas repetidas de los niveles de presión sonora al interior y exterior de los protectores auditivos, realizando mediciones de ruido bajo circunstancias habituales de trabajo para operadores de una planta de hidrocarburos.Posteriormente, se determinaron las diferencias entre los niveles de atenuación establecidos por el fabricante, los niveles de atenuación ajustados bajo el método recomendado por NIOSH y los niveles de atenuación obtenidos experimentalmente.Resultados Los valores de atenuación ofrecidos por los fabricantes difieren de los obtenidos experimentalmente, siendo estos últimos, menores en todos los casos del estudio. Los valores de atenuación de los protectores auditivos ajustados bajo el método NIOSH alcanzaron valores más cercanos a los experimentales.Conclusiones La variabilidad entre los valores de atenuación teóricos y experimentales, permiten establecer que las estimaciones de los niveles de atenuación obtenidos en condiciones controladas no consideran aspectos que en condiciones reales de uso afectan la eficiencia del protector auditivo. Este estudio plantea la necesidad de implementarprogramas integrales de protección auditiva, que permitan considerar variables asociadas a la eficacia del dispositivo en condiciones de uso, a través de la aplicación de pruebas de ajuste o en su defecto a través de la aplicación de los factores de ajuste sugeridos por NIOSH, con el fin de realizar una adecuada selección que permita alcanzar un control efectivo para el ruido.
ER  - 
TY  - JOUR
TI  - Hearing screening program impact on noise reduction strategies
AU  - Voaklander, D.C.
AU  - Franklin, R.C.
AU  - Challinor, K.
AU  - Depczynski, J.
AU  - JOUR
PB  - American Society of Agricultural and Biological Engineers
UR  - https://researchonline.jcu.edu.au/16658/1/Voaklander_et_al_2009.pdf
PY  - 2009
AB  - The objective of this study was to determine the impact of the New South Wales Rural Hearing Conservation Program on the implementation of personal hearing protection (PHP) and noise management strategies among farmers who had participated in this program in New South Wales, Australia. A follow-up survey of a random sample of people screened through the New South Wales Rural Hearing Conservation Program was linked to their baseline data. The use of PHP at baseline was compared to use at follow-up in four specific scenarios: use with non-cabbed tractors, with chainsaws, with firearms, and in workshops. For non-cabbed tractors, the net gain in PHP use was 13.3%; the net gain was 20.8% for chainsaws, 6.7% for firearms, and 21.3% for workshops. Older farmers and those with a family history of hearing loss were less likely to maintain or improve PHP use. Those with severe hearing loss, males, and participants reporting hearing problems in situations where background noise was present were more likely to maintain or improve PHP use. Forty-one percent of farmers had initiated other strategies to reduce noise exposure beyond the use of PHP, which included engineering, maintenance, and noise avoidance solutions. The early (hopefully) identification of hearing deficit in farmers and farm workers can help promote behavior change and help reinforce a farm culture that supports hearing conservation. The continuation and expansion of hearing screening programs such as these should be encouraged as basic public health strategy in farming communities.
ER  - 
TY  - JOUR
TI  - MP3 Players and Hearing Loss: Adolescents' Perceptions of Loud Music and Hearing Conservation:
AU  - Vogel, I.
AU  - Brug, J.
AU  - Hosli, E.J.
AU  - Ploeg, C.P.B. van der
AU  - Raat, H.
KW  - Health ; Jeugd en Gezondheid ; adolescent ; adult ; article ; female ; hearing ; hearing loss ; human ; loudness ; male ; music ; perception ; priority journal ; school child ; sound transmission ; Age Distribution ; Auditory Fatigue ; Auditory Threshold ; Child ; Cohort Studies ; Ear Protective Devices ; Focus Groups ; Noise-Induced ; Humans ; Incidence ; Netherlands ; Risk Assessment ; Rural Population ; Sex Distribution ; Tape Recording ; Urban Population ; Healthy for Life ; Healthy Living
UR  - http://resolver.tudelft.nl/uuid:460fb641-7f0f-46b6-b4f2-3938d959a2b8
LA  - en
PY  - 2008
AB  - Objective: To explore adolescents' behaviors and opinions about exposure to loud music from MP3 players. Study design: We conducted a qualitative analysis of focus-group discussions with adolescents aged 12 to 18 years from 2 large secondary schools (1 urban and 1 rural) for pre-vocational and pre-university education. The semi-structured question route was theoretically framed within the protection motivation theory. Results: Most adolescents-especially male students and students from pre-vocational schools-indicated that they often played their MP3 players at maximum volume. Although they appeared to be generally aware of the risks of exposure to loud music, they expressed low personal vulnerability to music-induced hearing loss. Most adolescents said that they would not accept any interference with their music-exposure habits. Conclusions: Interventions should target students from pre-vocational schools and should focus on increasing adolescents' knowledge of the risks of loud music and how to protect themselves. Besides hearing education for adolescents and technical modifications of MP3 players, volume-level regulations for MP3 players may be warranted. © 2008 Mosby, Inc. All rights reserved.
ER  - 
TY  - JOUR
TI  - Young People's Exposure to Loud Music. A Summary of the Literature:
AU  - Vogel, I.
AU  - Brug, J.
AU  - Ploeg, C.P.B. van der
AU  - Raat, H.
KW  - Health ; Jeugd en Gezondheid ; ear protection ; ethnicity ; exposure ; loudness ; physical activity ; review ; risk assessment ; sex difference ; social aspect ; Adolescent ; Adult ; Age Factors ; Attitude to Health ; Child ; Demography ; Environmental Exposure ; Female ; Hearing Loss ; Humans ; Male ; Music ; Noise ; Risk-Taking ; Socioeconomic Factors ; United States
UR  - http://resolver.tudelft.nl/uuid:271e3fc1-cad3-458c-b661-4a83215e5423
LA  - en
PY  - 2007
AB  - Objectives: This descriptive summary of the literature provides an overview of the available studies (published before October 2006) on sociodemographic, psychosocial, and other correlates of risk and protective behaviors for hearing loss in young people aged 12 to 25 years. Methods: Publications were identified by a structured search in PubMed, PsycINFO, and Web of Science, and by scrutinizing the reference lists of relevant articles. The protection motivation theory was used as the theoretical framework for categorizing the psychosocial correlates. Results: Thirty-three papers were included that identified several sociodemographic and psychosocial correlates, such as age, gender, school level, ethnicity, music preference, physical activity, social influence, and free supply of hearing protection. Conclusions: For the development of effective interventions we recommend theory-based longitudinal studies among those frequently exposed to loud music to assess these correlates in greater depth. © 2007 American Journal of Preventive Medicine.
ER  - 
TY  - JOUR
TI  - Discotheques and the risk of hearing loss among youth: Risky listening behavior and its psychosocial correlates:
AU  - Vogel, I.
AU  - Brug, J.
AU  - Ploeg, C.P.B. van der
AU  - Raat, H.
KW  - Health ; Jeugd en Gezondheid
UR  - http://resolver.tudelft.nl/uuid:5acb8b80-cdcc-48ca-a0b2-9ae20252a702
LA  - en
PY  - 2010
AB  - There is an increasing population at risk of hearing loss and tinnitus due to increasing high-volume music listening. To inform prevention strategies and interventions, this study aimed to identify important protection motivation theory-based constructs as well as the constructs 'consideration of future consequences' and 'habit strength' as correlates of adolescents' unsafe discotheque-visiting behavior. We invited 1687 adolescents (12-19 years old) at Dutch secondary schools to complete questionnaires about music-listening behaviors, sociodemographic characteristics and psychosocial determinants of behavior. Over 70% of participants reported to have visited discotheques; 24.6% of them were categorized as visitors at risk for hearing loss due to estimated exposure of 100 dBA for 1.25 hours per week or more without the use of hearing protection. Compared with visitors not at risk for hearing loss, those at risk were more likely not to live with both parents and less likely to consider future consequences and for them visiting high-volume music discotheques was more habitual. Risky exposure to high-volume music in discotheques is associated with several sociodemographic and psychosocial factors, with habit strength being the strongest correlate. Voluntary behavior change among adolescents might be difficult to achieve, because visiting discotheques seems to be strongly linked to current adolescent lifestyle. © The Author 2010. Published by Oxford University Press. All rights reserved.
ER  - 
TY  - JOUR
TI  - STUDI PERILAKU PENGGUNAAN EARPLUG PADA PEKERJA BAGIAN PRODUKSI DI PT X TAHUN 2019
AU  - Wau, Herbert
AU  - Harahap, M. Rizki Fadirah
AU  - JOUR
PB  - Prodi Kesehatan Masyarakat Fakultas Kesehatan Masyarakat UIN Sumatera Utara
UR  - http://jurnal.uinsu.ac.id/index.php/kesmas/article/view/7049
LA  - eng
PY  - 2020
AB  - PT X is a national private company engaged in agribusiness in the form of rubber processing with a noise level of 100.1 dB. PT.X found that there were 11 hearing impaired workers. Workers who are in a work environment with noise at> 85 dB do not use APT (Earplug) and are at risk for experiencing occupational diseases, namely deafness. The purpose of this study was to determine the Study of Behavior in the Use of APT (Earplug) in production section workers at PT X. This research is descriptive analytic, with a cross sectional research design. The population in this study were all workers who worked at PT X. The sample in this study was taken with a total sampling technique of 39 employees. Data collection techniques by observation and questionnaire. Analysis of the data used in this study was the Chi-Square test (95% CI). Based on the results of the Univariate test, it is known that the majority of respondents are 30-39 years old (16 people), the majority of high school / vocational school educated (26 people), the majority of respondent working duration> 2 years (36 people). Chi-Square Test results obtained that there is a relationship between attitude (p = 0.037) and action (p = 0.000) with the use of APT at PT X, while knowledge is not related to the use of APT at PT X, where, Knowledge with a value of p = 0.238, while the attitude with the use of APT value p = 0.037 and the action with a value of p = 0.000 so that there is a relationship between Attitudes and Actions towards the use of APT in Production Section Workers at PT X, Simalungun, 2019. The conclusion is there is no relationship between knowledge and the use of APT and there is relationship between attitude and action with the use of APT
ER  - 
TY  - JOUR
TI  - Why orchestral musicians are bound to wear earplugs: About the ineffectiveness of physical measures to reduce sound exposure
AU  - Wenmaekers, RHC Remy
AU  - Nicolai, B Bareld
AU  - Hornikx, MCJ Maarten
AU  - JOUR
UR  - http://repository.tue.nl/878515
LA  - en
PY  - 2017
AB  - Symphony orchestra musicians are exposed to noise levels that put them at risk of developing hearing damage. This study evaluates the potential effectivity of common control measures used in orchestras on open stages with a typical symphonic setup. A validated acoustic prediction model is used that calculates binaural sound exposure levels at the ears of all musicians in the orchestra. The model calculates the equivalent sound levels for a performance of the first 2 minutes of the 4th movement of Mahler’s 1st symphony, which can be considered representative for loud orchestral music. Calculated results indicate that risers, available space and screens at typical positions do not significantly influence sound exposure. A hypothetical scenario with surround screens shows that, even when shielding all direct sound from others, sound exposure is reduced moderately with the largest effect on players in loud sections. In contrast, a dramatic change in room acoustic conditions only leads to considerable reductions for soft players. It can be concluded that significant reductions are only reached with extreme measures that are unrealistic. It seems impossible for the studied physical measures to be effective enough to replace hearing protection devices such as ear plugs.
ER  - 
TY  - THES
TI  - Noise and Music - A Matter of Risk Perception?
AU  - Widén, Stephen E.
KW  - Adolescents ; Tinnitus ; Noise sensitivity ; Socio-economic status ; Attitudes ; Use of hearing protection ; Risk behaviour ; Risk-consideration ; Self-image ; Norms and Ideals
UR  - http://hdl.handle.net/2077/714
LA  - eng
PY  - 2006
AB  - The prevalence of tinnitus and hearing impairments among adolescents seems to increase as a consequence of exposure to loud noise. Several studies have highlighted the negative auditory effects of exposure to loud music at concerts and discotheques, environments in which young people today spend considerable periods of time. The appreciation of loud music clearly involves health-risks. Previous research suggests that patterns of health risk behaviours differ in relation to socio-economic status. The purpose of this thesis is to gain a better insight into adolescents’ and young adults’ attitudes and health-risk behaviours regarding exposure to loud music. Four empirical studies were conducted. Permanent tinnitus and noise sensitivity were not found to be significantly related to socio-economic status, although significant age-related differences in the prevalence of experienced tinnitus and noise sensitivity were found, which might indicate that the problem increases with age. Of 1285 subjects a larger number (30%) reported the use of hearing protection when attending concerts. Our finding that adolescents’ attitudes and behaviours regarding the use of hearing protection differed between levels of socio-economic status and age is of considerable interest. Adolescents from low socio- economic backgrounds express more positive attitudes towards noise and report less use of hearing protection, in comparison to those with high SES. These differences in attitudes and behaviour may indicate future socio-economic differences in ear health. Comparisons between Swedish and American young adults revealed that attitudes towards noise differed significantly due to gender and country. Men had more positive attitudes towards noise than women, and men from the USA had the most positive attitudes. Least positive were the women from Sweden. In Sweden the use of hearing protection at concerts was substantially higher than in the USA, a result that can be explained by cultural and attitudinal differences between the countries. Young people’s experiences, attitudes and beliefs concerning risk-taking in musical settings have been investigated in a qualitative study. In a theoretical model, we suggest that background variables, such as gender, culture and social status may have an impact on the individual’s self-image, risk consideration, social norms and ideals. These variables, together with attitudes and experience of risk-behaviour, are considered as important factors in the understanding of health-risk behaviour.
ER  - 
TY  - JOUR
TI  - Balancing speech intelligibility versus sound exposure in selection of personal hearing protection equipment for Chinook aircrews:
AU  - Wijngaarden, S.J. van
AU  - Rots, G.
KW  - aviation Acoustics and Audiology ; Aircrews speech transmission index ; Communication ; Earplugs ; Hearing protection ; Noise exposure ; Speech intelligibility ; airplane crew ; hearing impairment ; long term exposure ; performance ; protective equipment ; sound ; validation process ; Aerospace Medicine ; Ear Protective Devices ; Hearing Loss ; Noise-Induced ; Humans ; Materials Testing ; Occupational Diseases ; Occupational Health
UR  - http://resolver.tudelft.nl/uuid:b7896835-e211-4888-a280-5e32757c673a
LA  - en
PY  - 2001
AB  - Background: Aircrews are often exposed to high ambient sound levels, especially in military aviation. Since long-term exposure to such noise may cause hearing damage, selection of adequate hearing protective devices is crucial. Such devices also affect speech intelligibility. When speech intelligibility and hearing protection lead to conflicting requirements, a compromise must be reached. The selection of personal equipment for RNLAF Chinook aircrews is taken as an example of this process. Methods: Sound attenuation offered by aircrew helmets and ear plugs was measured using a standardized method. Sound attenuation results were used to calculate sound exposure. Objective predictions of speech intelligibility were calculated using the Speech Transmission Index (STI) method. Subjective preference was investigated through a survey among 28 experienced aircrew members. Results: The use of ear plugs in addition to a (RNLAF standard) helmet may lead to a significant reduction of sound exposure. Using ear plugs that offer high sound attenuation, instead of using a less attenuating type, gives a little additional reduction of sound exposure, at the expense of a large reduction in speech intelligibility. Hence, it is better to use 'light' ear plugs. Better performance still is offered by Communications Earplugs, ear plugs featuring integrated miniature earphones. Results from the user preference survey correspond well with objective measurement results. Conclusions: In the case of the RNLAF Chinook, the best solution is using Communications EarPlugs in combination with a standard helmet. The Chinook case clearly illustrates that hearing protection and speech intelligibility should be treated as connected issues.
ER  - 
TY  - JOUR
TI  - The audiological health of horn players
AU  - Wilson, Wayne J.
AU  - O'Brien, Ian
AU  - Bradley, Andrew P.
KW  - Hearing ; Horn ; Musicians ; Noise induced hearing loss ; Orchestra ; 2739 Public Health ; Environmental and Occupational Health
PB  - Taylor & Francis
UR  - https://espace.library.uq.edu.au/view/UQ:313204
LA  - eng
PY  - 2013
AB  - Among orchestral musicians, horn players are one of the most at-risk groups for noise-induced hearing loss (NIHL). To investigate this group further, pure tone audiometry and a 14-item questionnaire were used to assess the hearing health, as well as attitudes and practices regarding hearing conservation, among 142 French horn players attending an international horn conference in Brisbane, Australia. Of this study's French horn players, 11.1% to 22.2%, and 17.7% to 32.9% of those aged 40years, showed some form of hearing loss (corrected for age and gender) typical of NIHL, using conservative versus lenient criteria, respectively. Stepwise multiple regression analyses showed no obvious predictor of hearing loss in this study's participants. Of the 18% of participants who reported using hearing protection, 81% used this protection sometimes and 50% used generic, foam, or other inferior forms of protection. Continued efforts to better manage the hearing health of horn players is warranted particularly as any hearing loss will affect a horn player's ability to perform and therefore his or her livelihood. Managing the hearing health of horn players will be challenging, however, with no simple predictor of NIHL loss being identified in this study's sample.
ER  - 
